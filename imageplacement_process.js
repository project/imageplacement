
/**
 * Places images according to their saved position
 */

Drupal.imageplacement_process = {};

Drupal.behaviors.imageplacement_process = function() {
    Drupal.imageplacement_process.placeImages();
};

Drupal.imageplacement_process.placeImages = function() {
  var positions = Drupal.settings.imageplacement_positions;
  if (positions == undefined || positions.length <= 0) return false;

  var $clear = $('<div class="imageplacement_clear">&nbsp;</div>');

  $.each(positions, function(i) {
    if (this.teaser == 1) {
      var node_selector = '-teaser';
      var field_selector = ':first';
    }
    else {
      var node_selector = '';
      var field_selector = ':last';
    }

    var content_selector = '#node-' + this.nid + node_selector;

    var $image = $(content_selector + ' img[src$="' + encodeURIComponent(this.image) + '"]');
    if (this.grouped == 1) {
      var $image_field = $image.parents('.field-items').eq(0);
    }
    else {
      var $image_field = $image.parents('.field-item').eq(0);
    }
    var $content = $(content_selector);
    var $content_ps = $content.find('p');

    $content.append($clear);

    if (this.isleft == 1) {
      $image_field.addClass('imageplacement_left');
    }
    else {
      $image_field.addClass('imageplacement_right');
    }

    $content.find('p:eq(' + this.whichp + ')').before($image_field);

  });
};
