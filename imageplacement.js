
/**
 * Allows drag'n'drop placement of Imagefield/Imagecache images into nodes.
 */

Drupal.imageplacement = {};

Drupal.theme.imageplacementChangedWarning = function () {
  return '<div class="warning">' + Drupal.t("Changes made will not be saved until you click the Save button.") + '</div>';
};

Drupal.behaviors.imageplacement = function() {

  var fields = Drupal.settings.imageplacement_fields;
  if (fields == undefined || fields.length <= 0) return false;

  var changed = false,
      new_positions = {},
      whichp,
      halfwidth;

  var $clear = $('<div class="imageplacement_clear">&nbsp;</div>');

  $.each(fields, function() {
    var field = this;

    if (field.teaser) {
      var node_selector = '-teaser';
    }
    else {
      var node_selector = '';
    }

    var content_selector = '#node-' + field.nid + node_selector,
        images_selector = field.grouped ? content_selector + ' .' + field.field + ' .field-items' : content_selector + ' .' + field.field + ' .field-item',
        $image_field = $(content_selector + ' .' + field.field + ' .field-items'),
        $image_elements = $(images_selector),
        $content = $(content_selector),
        $content_ps = $content.find('p');

    $content.append($clear);

    $content_ps.droppable({
      hoverClass: 'imageplacement_p_hover',
      scope: content_selector,
      tolerance: 'pointer',
      over: function(e, ui) {
        halfwidth = (($(this).width() / 2) - (ui.draggable.width() / 2));
        whichp = $content_ps.index(this);
        ui.draggable.insertBefore(this);
      },
      out: function(e, ui) {
        $image_field.append(ui.draggable);
        whichp = 'none';
      }
    });

    $image_elements.draggable({
      containment: $content,
      refreshPositions: true,
      helper: 'clone',
      opacity: 0.7,
      scope: content_selector,
      start: function(e, ui) {
        halfwidth = (($content.width() / 2) - ($(this).width() / 2));
      },
      drag: function(e, ui) {
        // Make calculations if we apply left or right floats and where
        // we will be inserting the image in the text here
        Drupal.imageplacement.isLeft($(this), ui.position.left, halfwidth);
      },
      stop: function(e, ui) {
        // We set an adjusted width after depending on the floated field's width
        halfwidth = (($content.width() / 2) - ($(this).width() / 2));

        // Confirm position and save
        var position = {};
        position.field = field.field;
        position.image = Drupal.imageplacement.baseName($('img', ui.helper.context).attr('src'));
        position.left = Drupal.imageplacement.isLeft($(this), ui.position.left, halfwidth);
        position.nid = field.nid;
        position.teaser = field.teaser;
        position.whichp = whichp;
        position.grouped = field.grouped ? 1 : 0;

        if (changed == false) {
          $(Drupal.theme('imageplacementChangedWarning')).insertAfter($('div.messages')).hide().fadeIn('slow');
          changed = true;
        }

        var position_name = field.field + '_' + position.image.replace('_', '-') + '_' + field.nid + node_selector.replace('-', '_');
        new_positions[position_name] = position;

        $('#edit-imageplacement-positions').val(Drupal.imageplacement.param(new_positions));
      }
    });
  });

};

Drupal.imageplacement.isLeft = function($image, left, hw) {
  var is_left = 1;

  if (left <= hw || (left - hw) <= 50) {
    $image.addClass('imageplacement_left').removeClass('imageplacement_right');
    is_left = 1;
  }
  else {
    $image.addClass('imageplacement_right').removeClass('imageplacement_left');
    is_left = 0;
  }

  return is_left;
};

// From jQuery 1.7's param and buildParams functions
// Serialize an array of form elements or a set of
// key/values into a query string
Drupal.imageplacement.param = function( a, traditional ) {
  var r20 = /%20/g,
	    s = [],
  		add = function( key, value ) {
  			// If value is a function, invoke it and return its value
  			value = jQuery.isFunction( value ) ? value() : value;
  			s[ s.length ] = encodeURIComponent( key ) + "=" + encodeURIComponent( value );
  		};

	// Set traditional to true for jQuery <= 1.3.2 behavior.
	if ( traditional === undefined ) {
		traditional = jQuery.ajaxSettings.traditional;
	}

	// If an array was passed in, assume that it is an array of form elements.
	if ( jQuery.isArray( a ) || ( a.jquery && !jQuery.isPlainObject( a ) ) ) {
		// Serialize the form elements
		jQuery.each( a, function() {
			add( this.name, this.value );
		});

	} else {
		// If traditional, encode the "old" way (the way 1.3.2 or older
		// did it), otherwise encode params recursively.
		for ( var prefix in a ) {
			Drupal.imageplacement.buildParams( prefix, a[ prefix ], traditional, add );
		}
	}

	// Return the resulting serialization
	return s.join( "&" ).replace( r20, "+" );
};

Drupal.imageplacement.buildParams = function( prefix, obj, traditional, add ) {
	if ( jQuery.isArray( obj ) ) {
		// Serialize array item.
		jQuery.each( obj, function( i, v ) {
			if ( traditional || rbracket.test( prefix ) ) {
				// Treat each array item as a scalar.
				add( prefix, v );

			} else {
				// If array item is non-scalar (array or object), encode its
				// numeric index to resolve deserialization ambiguity issues.
				// Note that rack (as of 1.0.0) can't currently deserialize
				// nested arrays properly, and attempting to do so may cause
				// a server error. Possible fixes are to modify rack's
				// deserialization algorithm or to provide an option or flag
				// to force array serialization to be shallow.
				Drupal.imageplacement.buildParams( prefix + "[" + ( typeof v === "object" || jQuery.isArray(v) ? i : "" ) + "]", v, traditional, add );
			}
		});

	} else if ( !traditional && obj != null && typeof obj === "object" ) {
		// Serialize object item.
		for ( var name in obj ) {
			Drupal.imageplacement.buildParams( prefix + "[" + name + "]", obj[ name ], traditional, add );
		}

	} else {
		// Serialize scalar item.
		add( prefix, obj );
	}
};

Drupal.imageplacement.baseName = function(str) {
  var base = new String(str).substring(str.lastIndexOf('/') + 1);
  return base;
};
